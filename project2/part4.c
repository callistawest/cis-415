/*Description: This program will develop the fourth version of MCP,
 * 	adding a feature similar to "top", that will display
 * 	information about each processes that is being run.
 *	It will show that the processes are being stopped
 *	and continued after 1 second.
 *
 *
 *Author: Callista West
 *
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int currentprocesses;
int ctr;
pid_t *child_pids;

int *flag;
int check = 0;
int q;

/*Part 4 addition:
 * 	table that prints and updates
 * 	as each processes is run
 */

void table(pid_t current)
{
	char filename[1000];
	sprintf(filename, "/proc/%d/stat", current);
	FILE *f = fopen(filename, "r");
	int unused;
	char comm[1000];
	char state;
	int pid;
	int pid5, pid6, pid7;
	unsigned int pid8;
	unsigned long pid9, pid10, pid11, pid12, pid13, pid14;
	printf("------------------------------------\n");
	printf("Pid = %d\n", current);
	fscanf(f, "%d %s %c %d %d %d %d %u %lu %lu %lu %lu %lu %lu", &unused, comm, &state, &pid, &pid5, &pid6, &pid7, &pid8, &pid9, &pid10, &pid11, &pid12, &pid13, &pid14);
	printf("Filename: %s\n", comm);
	printf("State: %c\n", state);
	printf("Parent Pid: %d\n", pid);
	printf("Session ID of Process %d: %d\n", current, pid6);
	printf("Amount of time Process %d scheduled in user mode: %lu\n", current, pid14);
	printf("------------------------------------\n");
	fclose(f);
}



void alrm_handler(int sig)
{
	int status;
	if(flag[q] == 1)
	{
		kill(child_pids[q], SIGUSR1);
		printf("Stopping process %d...\n", child_pids[q]);
		alarm(1);
		kill(child_pids[q], SIGSTOP);
		if(flag[q+1] == 1)
		{
			printf("Continuing process %d...\n", child_pids[q+1]);
			kill(child_pids[q+1], SIGCONT);
		}
		else
		{
			printf("Continuing process %d...\n", child_pids[q]);
			kill(child_pids[q], SIGCONT);

		}
		if(waitpid(child_pids[q], &status, WNOHANG) != 0)
		{
			flag[q] = 0;
			check += 1;
		}
		if(flag[q] == 1)
			table(child_pids[q]);	
		q++;
		if(q == ctr)
			q = 0;
		if(check == ctr)
		{
			alarm(0);
		}
	}
	else
	{
		q++;
		if(q == ctr)
			q = 0;
		if(check == ctr)
		{
			alarm(0);
		}
		alarm(1);
	}
}

/*
 * Main Function
 * 	parses through input file
 * 	forks children
 * 	calls execvp on each command in input file
 *	calls alrm_handler using signal() and alarm()
 */

int main(int argc, char* argv[])
{
	FILE *input1 = fopen(argv[1], "r");
	ctr = 0;
	for( char c = getc(input1); c != EOF; c = getc(input1))
		if( c == '\n')
			ctr += 1;
	fclose(input1);
	flag = (int *)malloc(sizeof(int) * (ctr+1));
	for(int i= 0; i < ctr; i++)
		flag[i] = 1;
	flag[ctr] = 3;
	FILE *input = fopen(argv[1], "r");
	int s = 0;
	char *token;
	char* rest = NULL;
	int counter = 0;
	char *sentence;
	size_t sentsize = 1024;
	currentprocesses = ctr;
	sigset_t set, set2;
	int sig;
	int *sigptr = &sig;
	int status;
	int ttime[ctr];
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &set, NULL);
	sentence= (char *)malloc(sentsize * sizeof(char));
	child_pids = (pid_t *)malloc(sentsize * sizeof(pid_t));
	char **storing;
	storing = (char **)malloc(sentsize * sizeof(char*));
	char *first = (char *)malloc(sentsize * sizeof(char));
	if(input)
	{
		while( s < ctr)
		{
			for(int blank= 0; blank < 40; blank++)
				storing[blank] = '\0';
			getline(&sentence, &sentsize, input);
			char *newsent = sentence;
			if(newsent[strlen(newsent)-1] == '\n')
				newsent[strlen(newsent)-1] = '\0';
			int i = 0;
			for(token = strtok_r(newsent, " ", &newsent); token != NULL; token = strtok_r(NULL, " ", &newsent))
			{
				storing[i] = token;
				i++;
				
			}
			child_pids[counter] = fork();
			if(child_pids[counter] == 0)
			{
				sigwait(&set, sigptr);
				execvp(storing[0], storing);
				perror("");
				free(sentence);
				free(first);
				fclose(input);
				free(storing);
				free(child_pids);
				free(flag);
				exit(1);	
			}
			counter += 1;
			s += 1;;
		}
	}
	signal(SIGALRM, alrm_handler);
	alarm(1);
	q = 0;

	for(int t= 0; t < counter; t++)
		wait(0);

	//if (feof(input))
	//{
		free(sentence);
		free(first);
		fclose(input);
		free(flag);
		free(storing);
		free(child_pids);
		exit(0);
	//}

}
