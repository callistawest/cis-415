/*Description: This program will develop the first version of MCP
 * such that it can launch the workload and get all processes
 * running together.
 *
 *
 *Author: Callista West
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
	FILE *input1 = fopen(argv[1], "r");
	int ctr = 0;
	for( char c = getc(input1); c != EOF; c = getc(input1))
		if( c == '\n')
			ctr += 1;
	fclose(input1);
	FILE *input = fopen(argv[1], "r");
	int s = 0;
	char *token;
	char* rest = NULL;
	int counter = 0;
	size_t sentsize = 1024;
	char *sentence= (char *)malloc(1000 * sizeof(char));
	pid_t child_pids[ctr];
	char **storing;
	storing = (char **)malloc(20 * sizeof(char*));
	char *first = (char *)malloc(500 * sizeof(char));
	if(input)
	{
		while( s < ctr)
		{
			getline(&sentence, &sentsize, input);
			char *newsent = sentence;
			int i = 0;
			if(newsent[strlen(newsent)-1] == '\n')
				newsent[strlen(newsent)-1] = '\0';
			for(token = strtok(newsent, " "); token != NULL; token = strtok(NULL, " "))
			{
				storing[i] = token;
				//printf("storing[i] = %s\n", storing[i]);
				i++;
				
			}
			child_pids[counter] = fork();
			if(child_pids[counter] == 0)
			{
				(execvp(storing[0], storing));
				perror("");
				free(sentence);
				free(storing);
				free(first);
				fclose(input);
				exit(1);
				
				
			}
			counter += 1;
			for(int t = 0; t < i; t++)
			{
				storing[t] = '\0';
			}
			s += 1;
		}
	}
	for(int o= 0; o < counter; o++)
		wait(0);

		free(sentence);
		free(first);
		free(storing);
		fclose(input);
		exit(0);


}
