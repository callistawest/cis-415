/*Description: 
 * 	This program will develop the third version of MCP,
 * 	such that it will start and stop the processes to have them
 * 	execute in a Round Robin algorithm. This program uses signal()
 * 	alarm() and SIGCONT, SIGSTOP, and SIGUSR1 to control
 * 	when each process is running.
 *
 *
 *Author: Callista West
 *
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


//global variables to allow in all function's scope
int currentprocesses;
int ctr;
pid_t *child_pids;

int *flag;
int check = 0;
int q;

//unused function (delete later)
/*void sigalrm_handler(int signum)
{
	int status;
	int read = 0;
	while(check < 2)
	{
		int num = 0;
		if(flag[num] == 1)
		{
			printf("Sending signal SIGSTOP to child process %d...\n", child_pids[num]);
			kill(child_pids[num], SIGSTOP);
			printf("Sending signal SIGCONT to child process %d...\n", child_pids[num]);
			kill(child_pids[num], SIGCONT);
			while(waitpid(-1, &status, WNOHANG|WCONTINUED|WUNTRACED) > 0)
			{
				if(WIFEXITED(status))
				{
					flag[num] = 0;
					check += 1;
				}

			}
		}
		num += 1;
		printf("check = %d\n", check);
		for(int i = 0;i < ctr; i++)
			printf("flag[%d] = %d\n", i, flag[i]);
	}
	alarm(1);
}
*/

void alrm_handler(int sig)
{
	int status;
	if(flag[q] == 1)
	{
		printf("--------------------------------\n");
		kill(child_pids[q], SIGUSR1);
		if(check != ctr-1)
		{
			printf("Stopping process %d...\n", child_pids[q]);
			alarm(1);
			kill(child_pids[q], SIGSTOP);
		}
		if(flag[q+1] == 1)
		{
			printf("Continuing process %d...\n", child_pids[q+1]);
			kill(child_pids[q+1], SIGCONT);
		}
		else
		{
			printf("Continuing process %d...\n", child_pids[q]);
			kill(child_pids[q], SIGCONT);

		}
		if(waitpid(child_pids[q], &status, WNOHANG) != 0)
		{
			flag[q] = 0;
			check += 1;
		}
		q++;
		if(q == ctr)
			q = 0;
		if(check == ctr)
		{
			alarm(0);
		}
		printf("--------------------------------\n");
	}
	else
	{
		q++;
		if(q == ctr)
			q = 0;
		if(check == ctr)
		{
			alarm(0);
		}
		alarm(1);
	}
}


int main(int argc, char* argv[])
{
	FILE *input1 = fopen(argv[1], "r");
	ctr = 0;
	for( char c = getc(input1); c != EOF; c = getc(input1))
		if( c == '\n')
			ctr += 1;
	fclose(input1);
	flag = (int *)malloc(sizeof(int) * (ctr+1));
	for(int i= 0; i < ctr; i++)
		flag[i] = 1;
	flag[ctr] = 2;
	FILE *input = fopen(argv[1], "r");
	int s = 0;
	char *token;
	char* rest = NULL;
	int counter = 0;
	char *sentence;
	size_t sentsize = 1024;
	currentprocesses = ctr;
	sigset_t set, set2;
	int sig;
	int *sigptr = &sig;
	int status;
	int ttime[ctr];
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &set, NULL);
	sentence= (char *)malloc(sentsize * sizeof(char));
	child_pids = (pid_t *)malloc(sentsize * sizeof(pid_t));
	char **storing;
	storing = (char **)malloc(sentsize * sizeof(char*));
	char *first = (char *)malloc(sentsize * sizeof(char));
	if(input)
	{
		while( s < ctr)
		{
			for(int blank= 0; blank < 40; blank++)
				storing[blank] = '\0';
			getline(&sentence, &sentsize, input);
			char *newsent = sentence;
			if(newsent[strlen(newsent)-1] == '\n')
				newsent[strlen(newsent)-1] = '\0';
			int i = 0;
			for(token = strtok_r(newsent, " ", &newsent); token != NULL; token = strtok_r(NULL, " ", &newsent))
			{
				storing[i] = token;
				i++;
				
			}
			child_pids[counter] = fork();
			if(child_pids[counter] == 0)
			{
				sigwait(&set, sigptr);
				execvp(storing[0], storing);
				perror("");
				free(sentence);
				free(first);
				fclose(input);
				free(storing);
				free(child_pids);
				free(flag);
				exit(1);	
			}
			counter += 1;
			s += 1;;
		}
	}
	signal(SIGALRM, alrm_handler);
	alarm(1);
	q = 0;

	for(int t= 0; t < counter; t++)
		wait(0);

	//if (feof(input))
//	{
		free(sentence);
		free(first);
		fclose(input);
		free(flag);
		free(storing);
		free(child_pids);
		exit(0);
//	}

}
