/*Description: 
 * This program will develop the second version of MCP, building 
 * off of the first version. 
 * Child processes will wait for signal SIGUSR1 immediately after forking.
 * Then all child processes will be sent the SIGSTOP signal.
 * Then all child processes will be sent the SIGCONT signal.
 * The parent process waits for all children to terminate before starting.
 *
 *
 *Author: Callista West
 *
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void signaler(pid_t* pool, int size, int signal)
{
	for( int i= 0; i < size; i++)
	{
		kill(pool[i], signal);
		printf("Signal %d sent to child process %d\n", signal, pool[i]);
	}
	printf("----------------------------------\n");
}


int main(int argc, char* argv[])
{
	FILE *input1 = fopen(argv[1], "r");
	int ctr = 0;
	//ctr = number of lines in input file
	for( char c = getc(input1); c != EOF; c = getc(input1))
		if( c == '\n')
			ctr += 1;
	fclose(input1);
	FILE *input = fopen(argv[1], "r");
	int s = 0;
	char *token;
	char* rest = NULL;
	int counter = 0;
	char *sentence;
	size_t sentsize = 1024;
	sigset_t set;
	int sig;
	int *sigptr = &sig;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &set, NULL);
	sentence= (char *)malloc(sentsize * sizeof(char));
	pid_t child_pids[ctr];
	char **storing;
	storing = (char **)malloc(sentsize * sizeof(char*));
	char *first = (char *)malloc(sentsize * sizeof(char));
	if(input)
	{
		while( s < ctr)
		{
			for(int blank = 0; blank < 40; blank++)
				storing[blank] = '\0';
			getline(&sentence, &sentsize, input);
			char *newsent = sentence;
			int i = 0;
			if(newsent[strlen(newsent)-1] == '\n')
				newsent[strlen(newsent)-1] = '\0';	
			for(token = strtok_r(newsent, " ", &newsent); token != NULL; token = strtok_r(NULL, " ", &newsent))
			{
				storing[i] = token;
				i++;
				
			}
			child_pids[counter] = fork();
			if(child_pids[counter] == 0)
			{
				int value = sigwait(&set, sigptr);
				execvp(storing[0], storing);
				perror("");
				free(sentence);
				free(storing);
				free(first);
				fclose(input);
				exit(1);
				//error checking
			}
			counter += 1;
			s += 1;;
		}
	}

	//using signaler function to send signals to child processes
	signaler(child_pids, counter, SIGUSR1);
	signaler(child_pids,counter, SIGSTOP);
	signaler(child_pids,counter, SIGCONT);

	for(int t= 0; t < counter; t++)
		wait(0);

	//if (feof(input))
	//{
		free(sentence);
		free(first);
		free(storing);
		fclose(input);
		exit(0);
	//}


}
