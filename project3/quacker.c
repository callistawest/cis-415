/*=============================================================================
 * Program Name: lab7
 * Author: Callista West	
 * Date: 11/17/2020
 * Description:
 *     A simple program that implements a thread-safe queue of meal tickets
 *
 *===========================================================================*/

//========================== Preprocessor Directives ==========================
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <sys/time.h>
#include "string_parser.c"
//=============================================================================

//================================= Constants =================================
#define MAXNAME 15
#define MAXTOPICS 5
#define MAXTICKETS 3
#define MAXDISH 20
#define MAXPUBs 3
#define MAXSUBS 4
#define URLSIZE 300
#define CAPSIZE 400

//=============================================================================

//============================ Structs and Macros =============================
struct topicEntry {
        int entryNum;
        struct timeval timeStamp;
        int pubID;
        char *photoURL; //url to photo
        char *photoCaption;     //photo caption
        int length;
	//pthread_mutex_lock lock;
} topicEntry;

struct topicQueue {
	struct topicEntry buffer[URLSIZE];
	int head;
	int tail;
} topicQueue;

struct topicQueue topq[MAXTOPICS]; //INFO: Changed to be an array of structs


pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t pmutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t smutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t emutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t dmutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gmutex= PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t econd = PTHREAD_COND_INITIALIZER;
pthread_cond_t dcond = PTHREAD_COND_INITIALIZER;
pthread_cond_t pcond = PTHREAD_COND_INITIALIZER;

//declared globally
pthread_t publishers[20];
pthread_t subscribers[20];


int commandnum = -1;

//global variables	
int DELTA = 0;
int done = 1;
int done1 = 1;
int done2 = 1;
int done3 = 1;
int num[4] = {0, 0, 0, 0};
int q1 = 0;
int q2 = 0;
int q3 = 0;
int q4 = 0;

int loop1 = 0;
        int loop2 = 0;
        int loop3 = 0;
        int total = 0;
        int tqID[20];
        int inq[20];
        char *url[20];
	char *cap[20];
	int place = 0;
        int inqp = 0;
int totalget[50];
int gets1 = 0;

//=============================================================================

//================================= Functions =================================

int recent = 0;
int recent1 = 0;
int enqueue(struct topicEntry *TE)
{
	//printf("in the enqueue function\n");
	pthread_mutex_lock(&emutex);
	if(topq[tqID[recent]-1].buffer->pubID == 1)
	{
		if(q1 == 0)
			topq[(tqID[recent])-1].buffer[q1].entryNum == 0;
		else if(topq[(tqID[recent])-1].buffer[q1].entryNum >= topq[(tqID[recent])-1].buffer->length -1 )
		{
			//printf("reached max length\n");
			return 0;
		}		
		else{
			topq[(tqID[recent])-1].buffer[q1].entryNum = topq[(tqID[recent])-1].buffer[q1-1].entryNum + 1;
		}
		//printf("entryNum in enqueue %d\n", topq[(tqID[recent])-1].buffer[q1].entryNum);
		//printf("url: %s\n", topq[(tqID[recent])-1].buffer[q1].photoURL );
		topicQueue.tail += 1;
		gettimeofday(&(topq[(tqID[recent])-1].buffer[q1].timeStamp), NULL);
		q1++;
		recent++;
	}
	if(topq[(tqID[recent])-1].buffer->pubID == 2)
	{
		if(q2 == 0)
                        topq[(tqID[recent])-1].buffer[q2].entryNum == 0;

		if(topq[(tqID[recent])-1].buffer[q2].entryNum >= topq[(tqID[recent])-1].buffer->length -1)
		{
			//printf("reached max length\n");
			return 0;
		}		
		else{
			topq[(tqID[recent])-1].buffer[q2].entryNum = topq[(tqID[recent])-1].buffer[q2-1].entryNum + 1;
		}
		//printf("entryNum in enqueue %d\n", topq[(tqID[recent])-1].buffer[q2].entryNum);
		
		topicQueue.tail += 1;
		gettimeofday(&(topq[(tqID[recent])-1].buffer[q2].timeStamp), NULL);
		q2++;
		recent++;
	}
	if(topq[(tqID[recent])-1].buffer->pubID == 3)
        {
                if(q3 == 0)
		{
                        topq[(tqID[recent])-1].buffer[q3].entryNum = 0 ;
			//printf("first entry in the queue\n\n");
		}
		if(topq[(tqID[recent])-1].buffer[q3].entryNum >= topq[(tqID[recent])-1].buffer->length -1)
                {
                        //printf("reached max length\n");
                        return 0;
                }
		else{
                        topq[(tqID[recent])-1].buffer[q3].entryNum = topq[(tqID[recent])-1].buffer[q3-1].entryNum + 1;
                }
                //printf("entryNum in enqueue %d\n", topq[(tqID[recent])-1].buffer[q3].entryNum);

                topicQueue.tail += 1;
		gettimeofday(&(topq[(tqID[recent])-1].buffer[q3].timeStamp), NULL);
                q3++;
                recent++;
        }
	pthread_mutex_unlock(&emutex);
	return 1;
}

int topNum = 0;
int getting = 0;


int getting1 = 0;
int getEntry(int lastEntry, struct topicEntry TE, int quNum, int threadID)
{
	
	pthread_mutex_lock(&gmutex);
	//printf("in get entry\n");
	//printf("lastEntry: %d\n", lastEntry);
	//printf("quNum: %d\n", quNum);
	int leng = snprintf(NULL, 0, "%d", threadID);
	char* str = malloc(leng + 1);
	snprintf(str, leng + 1, "%d", threadID);
	//printf("converting to string\n");
	char subname[500];
	strcpy(subname ,"SUB:");
	strcat(subname,str);
	strcat(subname, ".txt");
	//printf("subname:\n %s\n\n", subname);
	free(str);
	FILE *output = fopen(subname, "w");
	
	//printf("entry num %d\n", topq[quNum-1].buffer[lastEntry].entryNum);
	//if(topq[quNum -1].buffer[lastEntry].entryNum == 0)
	if(topq[quNum -1].buffer[lastEntry-1].photoURL == NULL)
	{
		//printf("queue is empty\n");
		//printf("\n BUMMER \n");
		pthread_mutex_unlock(&gmutex);
		return 0;
	}
	else
	{
		for(int i = 0; i < lastEntry; i ++)
		{
			if(topq[quNum-1].buffer[i].entryNum == lastEntry-1 )
			{
				TE.entryNum = topq[quNum-1].buffer[lastEntry-1].entryNum;
				TE.timeStamp = topq[quNum -1].buffer[lastEntry-1].timeStamp;
				TE.pubID = topq[quNum -1].buffer->pubID;
				TE.photoURL = topq[quNum-1].buffer[lastEntry-1].photoURL;
				TE.photoCaption = topq[quNum-1].buffer[lastEntry-1].photoCaption;
				fprintf(output, "Entry num: %d\n", TE.entryNum);
				fprintf(output, "Timestamp: %ld\n", TE.timeStamp.tv_sec);
				fprintf(output, "PubID: %d\n", TE.pubID);
				fprintf(output, "%s\n", TE.photoURL);
				fprintf(output, "%s\n", TE.photoCaption);
				//printf("found last entry + 1\n");
				break;
			}
			else if(topq[quNum-1].buffer[i].entryNum-1 > lastEntry-1 )
			{
				TE.entryNum = topq[quNum-1].buffer[i].entryNum;
				TE.timeStamp = topq[quNum -1].buffer[i].timeStamp;
				TE.pubID = topq[quNum -1].buffer->pubID;
				TE.photoURL = topq[quNum-1].buffer[i].photoURL;
				TE.photoCaption = topq[quNum-1].buffer[i].photoCaption;
				fprintf(output, "%d\n", TE.entryNum);
				fprintf(output, "%ld\n", TE.timeStamp.tv_sec);
				fprintf(output, "%d\n", TE.pubID);
				fprintf(output, "%s\n", TE.photoURL);
				fprintf(output, "%s\n", TE.photoCaption);
				/*TE.entryNum = topq[getting].buffer[getting1].entryNum;
				TE.timeStamp = topq[getting].buffer[getting1].timeStamp;
				TE.pubID = topq[getting].buffer[getting1].pubID;
				printf("entry num getentry: %d\n",TE.entryNum);*/
				//strcpy(TE.photoURL, topq[getting].buffer[getting1].photoURL);
				//strcpy(TE.photoCaption, topq[getting].buffer[getting1].photoCaption);
				break;
			}
			
		}

	}
	fclose(output);
	//getting += 1;

	pthread_mutex_unlock(&gmutex);
	return 1;

}


void *dequeue(void *args) {
	sleep(DELTA);
	struct timeval currenttime;
	gettimeofday(&currenttime, NULL);
	for(int i = 0; i < topNum ; i++)
	{
		if((currenttime.tv_sec *1000000 + currenttime.tv_usec)- (topq[0].buffer[i].timeStamp.tv_sec * 1000000 + topq[0].buffer[i].timeStamp.tv_usec) > DELTA)
		{
			//printf("Dequeing: topic ID: %d\n", topq[0].buffer[i].pubID);
			for(int en = 0; en < topNum; en ++)
			{
				if(topq[0].buffer[i].entryNum < topq[0].buffer[en].entryNum)
				{
					topq[0].buffer[en].entryNum -= 1;
					topq[0].tail -= 1;
				}
			}
		}
		sched_yield();
	}
	
}
int pubnum = 0;
struct topicEntry *entry;
void *publisher(void *args) {
	
	entry = (struct topicEntry *)args;

	//printf("Publisher thread waiting %ld waiting for signal------------\n", pthread_self());
	pthread_mutex_lock(&pmutex);
	//printf("mutex lock publisher\n");	
	//printf("conditional wait publisher\n");	
	//enqueue(entry);
	//printf("enqueue complete -- \n\n\n");
	sched_yield();
	pthread_mutex_unlock(&pmutex);

}
int subget = 0;
void *subscriber(void *args) {
	entry = (struct topicEntry *)args;
	//printf("subscriber thread: %ld waiting for signal-------\n", pthread_self());
	pthread_mutex_lock(&smutex);	//locking for one thread at a time
	struct topicEntry firsttopic;
	//printf("subscriber thread: %ld dequeueing\n", pthread_self());
	unsigned int ID = pthread_self();
	//printf("thread ID: %d\n", ID);
	if(totalget[subget] == 1)
	{
		if(q1 >= topq[0].buffer->length)
			getEntry(topq[totalget[subget]-1].buffer->length, firsttopic, totalget[subget], ID);
		else
			
			getEntry(topq[totalget[subget]-1].buffer[q1].entryNum+1, firsttopic, totalget[subget], ID);
		q1--;
	}
	if(totalget[subget] == 2)
	{
		if(q2 >= topq[1].buffer->length)
			getEntry(topq[totalget[subget]-1].buffer->length, firsttopic, totalget[subget], ID);
		else
			
			getEntry(topq[totalget[subget]-1].buffer[q2].entryNum+1, firsttopic, totalget[subget], ID);
		q2 --;
	}
	if(totalget[subget] == 3)
	{
		if(q3 >= topq[2].buffer->length)
			getEntry(topq[totalget[subget]-1].buffer->length, firsttopic, totalget[subget], ID);
		else
			
			getEntry(topq[totalget[subget]-1].buffer[q3].entryNum+1, firsttopic, totalget[subget], ID);
		q3--;
	}
	subget++;
	//printf("subscriber finished  --\n");
	pthread_mutex_unlock(&smutex);	//unlockung for next thread
	sched_yield();
	sleep(1);
	
}

//called by add in input file for publishers only
int create(char *file)
{
	int l1 = 0;
	int l2 = 0;
	int l3 = 0;
	int l4 = 0;
	command_line command;
	size_t len = 64;
	char* line_buf = malloc(len);
	file++;
	file[strlen(file)-1] = '\0';
	FILE *fp = fopen(file, "r");
	int done = 1;
	int holder = 0;
	while(getline(&line_buf, &len, fp) != -1 && done == 1)
	{
		command = str_filler(line_buf, " ");
		for(int i = 0; command.command_list[i] != NULL; i++)
		{
			//printf("%s\n", command.command_list[i]);
			if(strcmp(command.command_list[i], "stop") == 0)
				done = 0;
			holder = i;
		}
		if(strcmp(command.command_list[0], "put") == 0)
                {
                	total += 1;
			commandnum += 1;
			tqID[place] = atoi(command.command_list[1]);
			place++;
			if(atoi(command.command_list[1]) == 1) 
			{
                                topq[atoi(command.command_list[1])-1].buffer[loop1].pubID = atoi(command.command_list[1]);
                                topq[atoi(command.command_list[1])-1].buffer[loop1].pubID = atoi(command.command_list[1]);
                                topq[atoi(command.command_list[1])-1].buffer[loop1].entryNum = loop1 -1;
				//printf("loop1: %d\n", loop1);
				inq[inqp] = inqp;
				l1++;
                          	inqp++;
				//printf("pubID: %d\n",topq[atoi(command.command_list[1])-1].buffer[loop1].pubID);
                                                        topq[atoi(command.command_list[1])-1].buffer[loop1].photoURL = (command.command_list[2]);
                                                        char capt[200];
                                                        strcpy(capt, command.command_list[3]);
                                                        //printf("caption: %s\n", capt);
							if(holder > 3)
                                                        {
                                                                for(int p = 4; p < holder+1; p++)
                                                                {
                                                                        strcat(capt, " ");
									strcat(capt, command.command_list[p]);
                                                                }
                                                                //topq[atoi(command.command_list[1])-1].buffer[loop1].photoCaption = (capt);
                                                        }
                                                       // printf("URL: %s\n", topq[atoi(command.command_list[1])-1].buffer[loop1].photoURL);
                                                        //printf("caption: %s\n", topq[atoi(command.command_list[1])-1].buffer[loop1-1].photoCaption);
                                                        topq[atoi(command.command_list[1])-1].buffer[loop1].photoCaption = capt;
                                                       // printf("caption: %s\n", topq[atoi(command.command_list[1])-1].buffer[loop1].photoCaption);
                                        		//pthread_create(&(publishers[commandnum]), NULL, publisher, topq);  
				loop1 ++;     
				}
		}
		if(strcmp(command.command_list[0], "put") == 0)
                {
                	if(atoi(command.command_list[1]) == 2) {
                                topq[atoi(command.command_list[1])-1].buffer[loop2].pubID = atoi(command.command_list[1]);
                                                        topq[atoi(command.command_list[1])-1].buffer[loop2].photoURL = (command.command_list[2]);
                                topq[atoi(command.command_list[1])-1].buffer[loop2].entryNum = loop2 -1;
				//printf("loop2 : %d\n", loop2);
				inq[inqp] = inqp;
				l2++;
				inqp++;
                                //printf("pubID: %d\n",topq[atoi(command.command_list[1])-1].buffer[loop2].pubID);
                                                        char capt[200];
                                                        strcpy(capt, command.command_list[3]);
                                                        //printf("caption: %s\n", capt);
							if(holder > 3)
                                                        {
                                                                for(int p = 4; p < holder+1; p++)
                                                                {
                                                                        strcat(capt, " ");
									strcat(capt, command.command_list[p]);
                                                                }
                                                                //topq[atoi(command.command_list[1])-1].buffer[loop1].photoCaption = (capt);
                                                        }
                                                        //printf("URL: %s\n", topq[atoi(command.command_list[1])-1].buffer[loop1].photoURL);
							//printf("caption: %s\n", capt);
                                                        topq[atoi(command.command_list[1])-1].buffer[loop2].photoCaption = (capt);
                                                        //topq[atoi(command.command_list[1])-1].buffer[loop2].photoURL = (command.command_list[2]);
                        loop2 ++;
			}
		}
		if(strcmp(command.command_list[0], "put") == 0)
                {
                	if(atoi(command.command_list[1]) == 3) {
                        	
                                topq[atoi(command.command_list[1])-1].buffer[loop3].pubID = atoi(command.command_list[1]);
                                                        topq[atoi(command.command_list[1])-1].buffer[loop3].photoURL = (command.command_list[2]);
                                topq[atoi(command.command_list[1])-1].buffer[loop3].entryNum = loop3 -1;
				//printf("loop3: %d\n", loop3);
				inq[inqp] = inqp;
				l3++;
				inqp++;
                                //printf("pubID: %d\n",topq[atoi(command.command_list[1])-1].buffer[loop3].pubID);
                                                        char capt[200];
                                                        strcpy(capt, command.command_list[3]);
                                                        //printf("caption: %s\n", capt);
							if(holder > 3)
                                                        {
                                                                for(int p = 4; p < holder+1; p++)
                                                                {
                                                                        strcat(capt, " ");
									strcat(capt, command.command_list[p]);
                                                                }
                                                                //topq[atoi(command.command_list[1])-1].buffer[loop1].photoCaption = (capt);
                                                        }
                                                        //printf("URL: %s\n", topq[atoi(command.command_list[1])-1].buffer[loop1].photoURL);
                                                        //printf("caption: %s\n", capt);
                                                        topq[atoi(command.command_list[1])-1].buffer[loop3].photoCaption = (capt);
                                                loop3 ++;
						}
		}
		if(strcmp(command.command_list[0], "sleep") == 0)
                {
			sleep(atoi(command.command_list[1])/ 1000);
		}
   //           free_command_line (&command);
     //         memset (&command, 0, 0);
	}
	free_command_line(&command);
	memset(&command, 0, 0);
	free(line_buf);
	fclose(fp);	
	return 1;
}

//called by add for subscribers only in input file
int create_sub(char *file)
{
	int l1 = 0;
	int l2 = 0;
	int l3 = 0;
	int l4 = 0;
	command_line command;
	size_t len = 64;
	char* line_buf = malloc(len);
	file++;
	file[strlen(file)-1] = '\0';
	FILE *fp = fopen(file, "r");
	int done = 1;
	int holder = 0;
	while(getline(&line_buf, &len, fp) != -1 && done == 1)
	{
		command = str_filler(line_buf, " ");
		for(int i = 0; command.command_list[i] != NULL; i++)
		{
			//printf("%s\n", command.command_list[i]);
			if(strcmp(command.command_list[i], "stop") == 0)
				done = 0;
			holder = i;
		}
		if(strcmp(command.command_list[0], "get") == 0)
		{
			totalget[gets1] = atoi(command.command_list[1]);
			gets1++;
		}
		//havent gotten sleep to work without stalling entire program indefinitely
		/*if(strcmp(command.command_list[0], "sleep") == 0)
                {
                        sleep((atoi(command.command_list[1]))/ 1000);
                }*/
              free_command_line (&command);
              memset (&command, 0, 0);

			
	}		
	
	free(line_buf);
	fclose(fp);	
	return 1;
}

int i = 0;
	char pubthreads[5][50];
	int pthread = 0;
	char subthreads[5][50];
	int sthread = 0;
//=============================================================================

//=============================== Program Main ================================
int main(int argc, char *argv[]) {
	//Variables Declarations
	command_line command;
      size_t len = 64;
      char* line_buf = malloc (len);
      pthread_t cleanup;
      
      
      pthread_t publisher1[10];
      pthread_t subscriber1[10];

      FILE* fp = fopen(argv[1], "r");
      while (getline (&line_buf, &len, fp) != -1)
      {
              command = str_filler (line_buf, " ");

              /*for (int i = 0; command.command_list[i] != NULL; i++)
              {
                      printf ("%s\n", command.command_list[i]);
	      }*/
	      if(strcmp(command.command_list[0], "create") == 0)
	      {
	      		struct topicEntry topic;
	      		topq[topNum].buffer->pubID = atoi(command.command_list[2]);
			topq[topNum].buffer->length = atoi(command.command_list[4]);
	      		topNum += 1;
			//printf("topNum: %d\n", topNum);
	      }
	      if(strcmp(command.command_list[0], "query") == 0)
	      {
	      		if(strcmp(command.command_list[1], "publishers") == 0)
			{
		        for(int i = 0; i < topNum; i++)
			{
				printf("Topic ID: %d Length: %d\n", topq[i].buffer[0].pubID,topq[i].buffer[0].length);
			}
			}
	      }
	      if(strcmp(command.command_list[0], "add") == 0)
	      {
			if(strcmp(command.command_list[1], "publisher") == 0)
			{
				create(command.command_list[2]);
			}
			else if(strcmp(command.command_list[1], "subscriber") == 0)
			{
				create_sub(command.command_list[2]);
			}

	      }
	      if(strcmp(command.command_list[0], "delta") == 0)
	      {
			DELTA = atoi(command.command_list[1]);
	      }
	      if(strcmp(command.command_list[0], "start") == 0)
	      {
		        pthread_create(&(cleanup), NULL, dequeue, topq);

			int pubflags[3];
			int subflags[3];
		/*	print statements for checking values throughout the program
		 *
		 *	printf("loop 1: %d\n", loop1);
			printf("loop 2: %d\n", loop2);
			printf("loop 3: %d\n\n\n", loop3);
			printf("total: %d\n", total);
          		
			for(i = 0; i < total; i++)
			{
				printf("tqID: %d\n", tqID[i]);
			}		
			for(i = 0; i < total; i++)
			{
				printf("inq: %d\n", inq[i]);
			}*/		
			for(i = 0; i < 5; i++)
			{
				for(int z = 0; z < total; z++)
				{
					topq[i].buffer[z].entryNum = 0;
				}
			}
			
			for(int i = 0; i < commandnum; i++)
			{
				pthread_create(&(publishers[i]), NULL, publisher, topq);
				pubflags[i] = 0;
			}
			for(int i = 0; i < 10; i++)
			{
				pthread_create(&(subscriber1[i]), NULL, subscriber, topq);
				subflags[i] = 0;
			}

			sleep(5);

			pthread_mutex_lock(&mutex);
			pthread_cond_broadcast(&cond1);
			pthread_mutex_unlock(&mutex);
	
			/*for(int i = 0; i < 10; i++)
			{
				pthread_join(subscribers[i], NULL);
			}*/
		      
	      
		}
              free_command_line (&command);
              memset (&command, 0, 0);
	      //free(line_buf1);
      }
	pthread_cond_destroy(&cond1);
	pthread_cond_destroy(&econd);
	pthread_cond_destroy(&dcond);
	pthread_cond_destroy(&pcond);

      free (line_buf);
	fclose(fp);	


	//STEP-5: Free the registry
	/*freeMTQ(0, "bfast");
	freeMTQ(1, "lnch");
	freeMTQ(2, "dnr");
	freeMTQ(3, "br");
	*/
	//return EXIT_SUCCESS;
	return 1;
}
//=============================================================================
