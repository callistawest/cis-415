/*
 * Description:
 * 	Program takes user input of an integer to create
 * 	that number of child processes using fork(). The program waits for all
 * 	child processes to finish. 
 *
 * Author: Callista West
 *
 * */



#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

void script_print(pid_t* pid_ary, int size)
{
        FILE* fout;
        fout = fopen("top_script.sh", "w");
        fprintf(fout, "#!/bin/bash\ntop");
        for(int i = 0; i < size; i++)
        {
                fprintf(fout, " -p %d", (int)(pid_ary[i]));
        }
        fprintf(fout, "\n");
        fclose(fout);

}



int main(int argc, char* argv[])
{
	int status;
	int x = atoi(argv[1]);
	pid_t child_pids[x];
	char* arg_list[] = {"./iobound", "-seconds", "5", NULL};
	for(int id =0; id< x; id++)
	{
		child_pids[id] = fork();
		if(child_pids[id] > 0)
		{
			continue;
		}
		if(child_pids[id] == 0)
		{	
			sleep(1);
			execvp(arg_list[0], arg_list);
			printf("error!\n");
		}
	}
	script_print(child_pids, x);
	
	for(int i = 0; i < x; i++)
		wait(0);
}

