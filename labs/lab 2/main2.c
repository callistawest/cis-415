/*
*Description:
        Program takes user an argument when ran (such as a text file) and tokenizes
        each line of the textfile. Each new line in the textfile begins
        with token 0 (T0). The program allows for users to continue to add input
        after the textfile has been read.
        Users can terminate the program by typing "exit" in the console.
*
*Author: Callista West
*
*Notes:
        User can add input after file has been read to tokenize their
        inputted sentence. IF the user presses enter, a new input line is
        created for the user to input on.
*
*
*/
/*-----------Preprocessor Directives-----*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*-----------------------------------------*/
/*------------Project Main----------------*/
int main(int argc, char **argv)
{
  /* Main function Variables */
  int counter = 0;
  int count = 0;
  int bufferlength = 250;
  char sentence[bufferlength];
  size_t sentsize = 1000;
  size_t characters;
  char *newsent;
  char* token;
  FILE* inputfile = fopen(argv[1], "r");
  if (inputfile == NULL)
      exit(EXIT_FAILURE);

    /* Allocate memory for the input buffer */
  newsent = (char *)malloc(sentsize * sizeof(char));
  char* rest = NULL;
  printf(">>> ");

  /* main run loop */
      /* Print >>> then get the input string */
      /* Tokenize the input string */u
      /* display each token */
      /* If the user entered <exit> then exit the loop */
while (count == 0)
{
  while(fgets(sentence, bufferlength, inputfile))
    {
      //printf("%s", sentence);
    //count is only changed to 1 when exit is typed by user
    if( count == 1)
      break;
    if(sentence[0] == '\n' /*| rest != NULL*/)
    {
      //if enter is not typed by user
      printf(">>> ");
      characters = getline(&newsent, &sentsize, stdin);
      counter = 0;
      //reset counter each time a new sentence is typed by user
    }
    if(sentence[0] != '\n' && count == 0)
    {
      if(sentence[0] == 'e' && sentence[1] == 'x' && sentence[2] == 'i' && sentence[3] == 't' && sentence[4] == '\n')
      {
        //checking if sentence equals "exit"
          count == 1;
          break;
      }
    }
      printf("\n");
      for(token = strtok_r(sentence, " ", &rest); token != NULL; token = strtok_r(NULL, " ", &rest))
        {
          printf("T%d: %s\n", counter, token);
          counter += 1;
        }
        //reset counter for each line of the text file
        counter = 0;
  }
  //breaks out of while loop and allows for user to type new lines to be tokenized
  // or to type exit to terminate the program
  if(newsent[0] == '\n' | rest != NULL)
  {
    //if enter is not typed by user
    printf(">>> ");
    characters = getline(&newsent, &sentsize, stdin);
    counter = 0;
    //reset counter each time a new sentence is typed by user
  }
  if(newsent[0] != '\n' && count == 0)
  {
    if(newsent[0] == 'e' && newsent[1] == 'x' && newsent[2] == 'i' && newsent[3] == 't' && newsent[4] == '\n')
    {
      //checking if sentence equals "exit"
        count == 1;
        break;
    }
    printf("\n");
    for(token = strtok_r(newsent, " ", &rest); token != NULL; token = strtok_r(NULL, " ", &rest))
      {
        printf("T%d: %s\n", counter, token);
        counter += 1;
      }
  }

}

  /*Free allocated memory*/
  /*close text file that was read*/
  fclose(inputfile);
  free(newsent);
  return 1;
}
/*------------------Program End-------------*/
