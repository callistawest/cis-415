/*
*Description:
        Program takes user input and separates by the input into tokens broken by spaces.
        The tokens are numbered by how the occur in the users original input.
        Program exits when "exit" is typed and produces a new line for input when
          the enter key is pressed.
*
*Author: Callista West
*
*Notes:
*
*
*/
/*-----------Preprocessor Directives-----*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*-----------------------------------------*/
/*------------Project Main----------------*/
int main()
{
  /* Main function Variables */
  int counter = 0;
  int count = 0;
  char *sentence;
  size_t sentsize = 1000;
  size_t characters;
  char* token;
    /* Allocate memory for the input buffer */
  sentence = (char *)malloc(sentsize * sizeof(char));
  char* rest = NULL;
  printf(">>> ");
  characters = getline(&sentence, &sentsize, stdin);
  //printf("\n");





  /* main run loop */
      /* Print >>> then get the input string */
      /* Tokenize the input string */
      /* display each token */
      /* If the user entered <exit> then exit the loop */

while(count == 0 )
{

  //count is only changed to 1 when exit is typed by user
  if(sentence[0] == '\n' | rest != NULL)
  {
    //if enter is typed by user
    printf(">>> ");
    characters = getline(&sentence, &sentsize, stdin);
    counter = 0;

    //reset counter each time a new sentence is typed by user
  }
  if(sentence[0] != '\n' && count == 0)
  {
    if(sentence[0] == 'e' && sentence[1] == 'x' && sentence[2] == 'i' && sentence[3] == 't' && sentence[4] == '\n')
    {
      //checking if sentence equals "exit"
        count == 1;
        break;
    }
    printf("\n");
    for(token = strtok_r(sentence, " ", &rest); token != NULL; token = strtok_r(NULL, " ", &rest))
      {
        printf("T%d: %s", counter, token);
        counter += 1;
        if(token != NULL)
            printf("\n");
      }
  }


}
  /*Free allocated memory*/
  free(sentence);
  return 1;
}
/*------------------Program End-------------*/
