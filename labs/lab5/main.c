/*
 * Description:
 * 	Program takes user input of an integer to create
 * 	that number of child processes using fork(). The program waits for all
 * 	child processes to finish. The program also uses signals such as SIGUSR1	, SIGSTOP, SIGCONT, and SIGINT to send signals to the child processes.
 * 	All children are terminated before the end of the program by the SIGINT
 * 	signal sent to the children processes. 
 *
 * Author: Callista West
 *
 * */


#define _OPEN_THREADS
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>

void script_print(pid_t* pid_ary, int size)
{
        FILE* fout;
        fout = fopen("top_script.sh", "w");
        fprintf(fout, "#!/bin/bash\ntop");
        for(int i = 0; i < size; i++)
        {
                fprintf(fout, " -p %d", (int)(pid_ary[i]));
        }
        fprintf(fout, "\n");
        fclose(fout);

}

//signaler function called by main
//where for the number of child processes, the print statement will print
//followed by the sending of the signal through kill().
void signaler(pid_t* pool, int size, int signal)
{
	sleep(2);
	for(int i = 0; i < size; i++)
	{
		printf("Parent process: %d - Sending signal: %d to child process: %d\n", getpid(), signal, pool[i]);
		kill(pool[i], signal);
	}

}



int main(int argc, char* argv[])
{
	int status;
	sigset_t set;
	int sig;
	int *sigptr = &sig;
	int value;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &set, NULL);
	int x = atoi(argv[1]);
	pid_t child_pids[x];
	char* arg_list[] = {"./iobound", "-seconds", "5", NULL};
	for(int id =0; id< x; id++)
	{
		child_pids[id] = fork();
		if(child_pids[id] > 0)
		{
			continue;
		}
		if(child_pids[id] == 0)
		{	
			printf("Child Process: %d - Waiting for SIGUSR1...\n", getpid());
			value = sigwait(&set, sigptr);
			if(value == -1)
				printf("error\n");
			else
			{
				if(*sigptr == 10)
					printf("Child Process: %d - Received signal: SIGUSR1 - Calling exec().\n", getpid());
			}
			execvp(arg_list[0], arg_list);
			printf("error!\n");
		}

	}
	//sending signals in expected order to signaler function, where
	//signals are delivered using the kill() function
	signaler(child_pids, x, SIGUSR1);
	signaler(child_pids, x, SIGSTOP);
	sleep(5);
	signaler(child_pids, x, SIGCONT);
	sleep(3);
	signaler(child_pids, x, SIGINT);
	
	for(int i = 0; i < x; i++)
		wait(0);

}
