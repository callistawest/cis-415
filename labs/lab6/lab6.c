/* Description:
 *	This program creates a mealTicket queue, which keep track of
 *	different meal items within the 4 meal options: Breakfast,
 *	Lunch, Dinner, and Bar. The MTQ struct and the mealTicket struct 
 *	include specific information about the meals and meal categories.
 *
 *	The main function calls the enqueue() and dequeue() function.
 *
 *	I modified my MTQ struct from the given code to eliminate const
 *	variables.
 *
 *Author: Callista West
 */




#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXNAME 200
#define MAXQUEUES 5

struct mealTicket {
	int ticketNum;
	char* dish_name;
};

struct MTQ {
	char name[MAXNAME];
	struct mealTicket * buffer;
	int head;
	int tail;
	int length;
};


struct MTQ *registry[MAXQUEUES];
int breaknum = 0;
int lunchnum = 0;
int dinnum = 0;
int barnum = 0;

int enqueue(char *MTQ_ID,  struct mealTicket *MT)
{
	if(strcmp(MTQ_ID, "Breakfast") == 0)
	{
		if(registry[0]->head == 3)
		{
			printf("pushing: Queue: %s - Error Queue is full.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[0]->buffer = MT;
			registry[0]->head += 1;
			MT->ticketNum = breaknum;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
			breaknum += 1;
		}
	}
	
	if(strcmp(MTQ_ID, "Lunch") == 0)
	{
		if(registry[1]->head == 3)
		{
			printf("pushing: Queue: %s - Error Queue is full.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[1]->buffer = MT;
			registry[1]->head += 1;
			MT->ticketNum = lunchnum;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
			lunchnum += 1;
		}
	}
	if(strcmp(MTQ_ID, "Dinner") == 0)
	{
		if(registry[2]->head == 3){
			printf("pushing: Queue: %s - Error Queue is full.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[2]->buffer = MT;
			//registry[0][registry[0]->head] = MT;
			registry[2]->head += 1;
			MT->ticketNum = dinnum;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
			dinnum += 1;
		}
	}
	if(strcmp(MTQ_ID, "Bar") == 0)
	{
		if(registry[3]->head == 3){
			printf("pushing: Queue: %s - Error Queue is full.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[3]->buffer = MT;
			//registry[0][registry[0]->head] = MT;
			registry[3]->head += 1;
			MT->ticketNum = barnum;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
			barnum += 1;
		}
	}
}

int dequeue(char *MTQ_ID, struct mealTicket *MT)
{
	if(strcmp(MTQ_ID, "Breakfast") == 0)
	{
		if(registry[0]->head == 0)
		{
			printf("popping: Queue: %s - Queue is empty, nothing to pop.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[0]->buffer = MT;
			registry[0]->head -= 1;
			//MT->ticketNum = breaknum;
			printf("popping: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
			//breaknum = 1;
		}
	}
	
	if(strcmp(MTQ_ID, "Lunch") == 0)
	{
		if(registry[1]->head == 0)
		{
			printf("popping: Queue: %s - Queue is empty, nothing to pop.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[1]->buffer = MT;
			registry[1]->head -= 1;
			printf("popping: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
		}
	}
	if(strcmp(MTQ_ID, "Dinner") == 0)
	{
		if(registry[2]->head == 0){
			printf("popping: Queue: %s - Queue is empty, nothing to pop.\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[2]->buffer = MT;
			registry[2]->head -= 1;
			printf("popping: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
		}
	}
	if(strcmp(MTQ_ID, "Bar") == 0)
	{
		if(registry[3]->head == 0){
			printf("popping: Queue: %s - Queue is empty, nothing to pop\n", MTQ_ID);
			return 0;
		}
		else
		{
			registry[3]->buffer = MT;
			registry[3]->head -= 1;
			printf("popping: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT-> dish_name);
		}
	}
	return 1;
}

int main()
{
	struct MTQ Breakfast;
	struct MTQ Lunch;
	struct MTQ Dinner;
	struct MTQ Bar;
	struct MTQ hold[1] = { Breakfast};
	struct MTQ hold1[1] = { Lunch};
	struct MTQ hold2[1] = { Dinner};
	struct MTQ hold3[1] = { Bar};
	registry[0] = hold;
	registry[1] = hold1;
	registry[2] = hold2;
	registry[3] = hold3;
	registry[0]->head = 0;
	registry[0]->tail = 0;
	registry[1]->head = 0;
	registry[1]->tail = 0;
	registry[2]->head = 0;
	registry[2]->tail = 0;
	registry[3]->head = 0;
	registry[3]->tail = 0;
	strcpy(registry[0]->name, "Breakfast");
	registry[0]->length = 3;
	strcpy(registry[1]->name, "Lunch");
	registry[1]->length = 3;
	strcpy(registry[2]->name, "Dinner");
	registry[2]->length = 3;
	strcpy(registry[3]->name, "Bar");
	registry[3]->length = 3;
	
	struct mealTicket pancakes[3];
	pancakes->ticketNum = 0;
	pancakes->dish_name = "pancakes";
	enqueue(registry[0]->name, pancakes);
	
	struct mealTicket eggs[3];
	eggs->ticketNum = 0;
	eggs->dish_name = "eggs";
	enqueue(registry[0]->name, eggs);
	
	struct mealTicket omlette[3];
	omlette->ticketNum = 0;
	omlette->dish_name = "omlette";
	enqueue(registry[0]->name, omlette);
	
	struct mealTicket waffle[3];
	waffle->ticketNum = 0;
	waffle->dish_name = "waffle";
	enqueue(registry[0]->name, waffle);
	
	struct mealTicket sandwich[3];
	sandwich->ticketNum = 0;
	sandwich->dish_name = "sandwich";
	enqueue(registry[1]->name, sandwich);
	
	struct mealTicket wrap[3];
	wrap->ticketNum = 0;
	wrap->dish_name = "wrap";
	enqueue(registry[1]->name, wrap);
	
	struct mealTicket quesadilla[3];
	quesadilla->ticketNum = 0;
	quesadilla->dish_name = "quesadilla";
	enqueue(registry[1]->name, quesadilla);
	
	struct mealTicket soup[3];
	soup->ticketNum = 0;
	soup->dish_name = "soup";
	enqueue(registry[1]->name, soup);
	
	struct mealTicket burger[3];
	burger->ticketNum = 0;
	burger->dish_name = "burger";
	enqueue(registry[2]->name, burger);
	
	struct mealTicket pasta[3];
	pasta->ticketNum = 0;
	pasta->dish_name = "pasta";
	enqueue(registry[2]->name, pasta);
	
	struct mealTicket tacos[3];
	tacos->ticketNum = 0;
	tacos->dish_name = "tacos";
	enqueue(registry[2]->name, tacos);
	
	struct mealTicket salad[3];
	salad->ticketNum = 0;
	salad->dish_name = "salad";
	enqueue(registry[2]->name, salad);
	
	struct mealTicket soda[3];
	soda->ticketNum = 0;
	soda->dish_name = "soda";
	enqueue(registry[3]->name, soda);
	
	struct mealTicket wine[3];
	wine->ticketNum = 0;
	wine->dish_name = "wine";
	enqueue(registry[3]->name, wine);
	
	struct mealTicket beer[3];
	beer->ticketNum = 0;
	beer->dish_name = "beer";
	enqueue(registry[3]->name, beer);
	
	struct mealTicket juice[3];
	juice->ticketNum = 0;
	juice->dish_name = "juice";
	enqueue(registry[3]->name, juice);

	printf("\n");

	dequeue(registry[0]->name, pancakes);
	dequeue(registry[0]->name, eggs);
	dequeue(registry[0]->name, omlette);
	dequeue(registry[0]->name, waffle);
	
	dequeue(registry[1]->name, sandwich);
	dequeue(registry[1]->name, wrap);
	dequeue(registry[1]->name, quesadilla);
	dequeue(registry[1]->name, soup);
	
	dequeue(registry[2]->name, burger);
	dequeue(registry[2]->name, pasta);
	dequeue(registry[2]->name, tacos);
	dequeue(registry[2]->name, salad);
	
	dequeue(registry[3]->name, soda);
	dequeue(registry[3]->name, wine);
	dequeue(registry[3]->name, beer);
	dequeue(registry[3]->name, juice);
	
	return 1;
}


