#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

/*-----------------------------------------*/
int lfcat()
{
  int count = 1;
  char cwd[1024];
  getcwd(cwd, sizeof(cwd));
  
  //opening current working directory
  DIR *dr = opendir(cwd);
  
  //redirecting stdout to output.txt
  FILE *fp;
  fp = freopen("output.txt", "w+", stdout);
  ssize_t nread;
  size_t len = 1000;
  struct dirent *de;
  while((de = readdir(dr)))
  {
      size_t len = 1000;
      char *line = malloc(sizeof(char) * len);
     
      FILE *dif_file = fopen(de->d_name, "r");

      //excludes files in the directory that should not be written to output.txt
      //include output.txt that would cause computer to crash
      // checks for ".output.txt.swp\0" due to duplicate output.txt swap files occuring
      // checks for null character at end of each file name ("\0")

      if(strcmp(de->d_name, "command.c\0") != 0 && strcmp(de->d_name, "lab3make\0") != 0 && strcmp(de->d_name, ".output.txt.swp\0") != 0 && strcmp(de->d_name, ".\0") != 0 && strcmp(de->d_name, "..\0") != 0 && strcmp(de->d_name, "command.o\0") != 0 && strcmp(de->d_name, "command.h\0") != 0 && strcmp(de->d_name, "main.c\0") != 0 && strcmp(de->d_name, "main.o\0") != 0 && strcmp(de->d_name, "makefile\0") != 0 && strcmp(de->d_name, "a.out\0") != 0 && strcmp(de->d_name, "output.txt\0") != 0)
    {
      //writes filename followed by new line character
      write(count, "File: ", strlen("File: "));
      write(count, (de->d_name), strlen(de->d_name));
      write(count, "\n", strlen("\n"));
      int new = 1;
      while(new = 1)
      {
	//while loop to write each line of each file
	nread = getline(&line, &len, dif_file);
	//getline returns -1 once no lines remain in the file
	if(nread == -1)
	{
		new = 0;
		break;
	}
	else
	{
		char *lines = line;
		write(count, lines, strlen(lines));
		write(count, "\n", strlen("\n"));
	}
      }
      
      //write 80 "-" as directed following each file
      write(count, "--------------------------------------------------------------------------------", 80);
      write(count, "\n", strlen("\n"));


    }
    //closing files and free space from mallocs
    fclose(fp);
    fclose(dif_file);
    free(line);
    }
  
}
