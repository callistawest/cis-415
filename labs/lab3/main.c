/*
*Description:
        This lab takes user input. When the user types "lfcat", the
 *      program calls the file command.c, where all contents of all the files
 *      in the current working direct to the specified output file,
 *      output.txt. If "lfcat" is not typed, an error will appear.
*
*Author: Callista West
*
*Notes:
*	Program ends after "lfcat" is sent by user into the command line.
*	Can access command.c (where lfcat() is located through the line 
*	#include "command.h".
*
*/
/*-----------Preprocessor Directives-----*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include "command.h"


/*-----------------------------------------*/
/*------------Project Main----------------*/
int main()
{
  /* Main function Variables */
  int counter = 0;
  char *sentence;
  size_t sentsize = 1000;
  int characters;
  sentence = (char *)malloc(sentsize * sizeof(char));
  char* rest = NULL;
  int start = 1;
  //loop to keep running until "lfcat" is entered
  while(start == 1)
  {  
  	printf(">>> ");
  	characters = getline(&sentence, &sentsize, stdin);
  	char *newsent = sentence;
	//account for "\n" character following "lfcat" 
	//when user presses enter
  	if(strcmp(&newsent[0], "lfcat\n") != 0)
	{
    		free(sentence);
  		printf("Error: Unrecognized command!\n");
	}
        else
  	{
		lfcat();
		free(sentence);
		return 1;
  	}
  }


}
