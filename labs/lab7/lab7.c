/*=============================================================================
 * Program Name: lab7
 * Author: Callista West	
 * Date: 11/17/2020
 * Description:
 *     A simple program that implements a thread-safe queue of meal tickets
 *
 *===========================================================================*/

//========================== Preprocessor Directives ==========================
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
//=============================================================================

//================================= Constants =================================
#define MAXNAME 15
#define MAXQUEUES 4
#define MAXTICKETS 3
#define MAXDISH 20
#define MAXPUBs 3
#define MAXSUBS 4
//=============================================================================

//============================ Structs and Macros =============================
typedef struct mealTicket{
	int ticketNum;
	char *dish;
} mealTicket;

//TODO: Declare a mutex in the struct. (e.g. add pthread_mutex_t ...)
typedef struct MTQ {
	char name[MAXNAME];
	struct mealTicket *buffer;
	int head;
	int tail;
	int max_length;
	int length;
	int ticket;
	int thread;
	pthread_mutex_t lock;
} MTQ;

/*typedef struct arg {
	struct mealTicket *mtarray;
	char mtq_id[MAXNAME];
	int thread;
} arg;*/

pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t emutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t pmutex= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t dmutex= PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t econd = PTHREAD_COND_INITIALIZER;
pthread_cond_t dcond = PTHREAD_COND_INITIALIZER;
pthread_cond_t pcond = PTHREAD_COND_INITIALIZER;


//TODO: Add a mutex init to this function (e.g registry[pos].mutex = PTHREAD_MUTEX_INITIALIZER)
//TODO: Add a conditional mutex, use pthread_cond_wait and pthread_cond_broadcast to make all threads hold until they are all ready to start together.

//A good example link is posted here
//https://www.geeksforgeeks.org/condition-wait-signal-multi-threading/

//It will be really helpeful for you to add a print entry function for debug and demonstration purpose.
MTQ registry[MAXQUEUES]; //INFO: Changed to be an array of structs


void init(int pos, char *MTQ_ID) {
    //....................
    //....................
    //....................
    //....................
    registry[pos].lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
    //pthread_cond_wait(&cond1, &registry[pos].lock);
    //pthread_cond_broadcast(&cond1);
}

void freeMTQ(int pos, char *MTQ_ID) {
	//....................
	pthread_cond_destroy(&cond1);
}
	
int done = 1;
int done1 = 1;
int done2 = 1;
int done3 = 1;
int num[4] = {0, 0, 0, 0};
int b = 0;
int l = 0;
int d = 0;
int r = 0;

//=============================================================================

//================================= Functions =================================
int enqueue(char *MTQ_ID, mealTicket *MT)
{
	//Step-1: Find registry
	int place;
	if(strcmp(MTQ_ID, "Breakfast") == 0)
		place = 0;
	if(strcmp(MTQ_ID, "Lunch") == 0)
		place = 1;
	if(strcmp(MTQ_ID, "Dinner") == 0)
		place = 2;
	if(strcmp(MTQ_ID, "Bar") == 0)
		place = 3;
	//STEP-2: 
		//TODO: Aquire the lock if it's available. Otherwise, wait until it is.
		//HINT: Use conditional variables and sched_yield.
	//if(done1 == 1)
	//{
	//	done1 = 2;
	//	printf("waiting on condition variable cond1\n");
		//pthread_cond_wait(&econd, &(registry[place].lock));
	//}
	//else
	//{
	//	printf("signaling condition variable cond1\n");
	//	pthread_cond_broadcast(&cond1);
	//	pthread_cond_signal(&cond1);
	//}
	//SETP-3: enqueue
	pthread_mutex_lock(&emutex);
	pthread_mutex_unlock(&emutex);
	if(registry[place].head == 3)
	{
		printf("pushing: Queue is full\n");
		return 0;
	}
	else
	{
		if(place == 0)
		{
			registry[place].buffer = MT;
			registry[place].head += 1;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, registry[place].buffer[b].dish);
			b += 1;
			MT->ticketNum += 1;
		}
		if(place == 1)
		{
			registry[place].buffer = MT;
			registry[place].head += 1;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, registry[place].buffer[l].dish);
			l += 1;
			MT->ticketNum += 1;		
		}
		if(place == 2)
		{
			registry[place].buffer = MT;
			registry[place].head += 1;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, registry[place].buffer[d].dish);
			d += 1;
			MT->ticketNum += 1;
		}
		if(place == 3)
		{
			registry[place].buffer = MT;
			registry[place].head += 1;
			printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, registry[place].buffer[r].dish);
			r += 1;
			MT->ticketNum += 1;
		}
        //....................
	//TODO: Release the lock.
	}	
	//pthread_mutex_unlock(&(registry[place].lock));

	return 1;
}

int dequeue(char *MTQ_ID, int ticketNum, mealTicket *MT) {
	//Step-1: Find registry
	int place;
	if(strcmp(MTQ_ID, "Breakfast") == 0)
		place = 0;
	if(strcmp(MTQ_ID, "Lunch") == 0)
		place = 1;
	if(strcmp(MTQ_ID, "Dinner") == 0)
		place = 2;
	if(strcmp(MTQ_ID, "Bar") == 0)
		place = 3;
	//Step-2:
		//TODO: Aquire the lock if it's available. Otherwise, wait until it is.
		//HINT: Use conditional variables and sched_yield.
	pthread_mutex_lock(&dmutex);
	pthread_mutex_unlock(&dmutex);
	/*if(done2 == 1)
	{
		done2 = 2;
		//printf("waiting on condition variable cond1\n");
		pthread_cond_wait(&cond1, &(registry[place].lock));
	}
	else
	{
		//printf("signaling condition variable cond1\n");
		pthread_cond_signal(&cond1);
	}
	sched_yield();
	pthread_mutex_unlock(&registry[place].lock);*/

	//Setp-3: dequeue
	    //....................
	    //....................
	if(registry[place].head == 0)
	{
		printf("popping: Queue is empty\n");
		return 0;
	}
	else
	{
		registry[place].buffer = MT;
		registry[place].head -= 1;
		printf("popping: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_ID, MT->ticketNum, MT->dish);
	}
	done2 = 1;
	//Step-4: 
		//TODO: Release the lock.
	
	return 1;
}

struct MTQ *meal;

int t = 0;
int b1 = 0;
int b2 = 0;
int b3 = 0;
int i = 0;
int i2 = 0;
int i3 = 0;
int i4 = 0;
pthread_cond_t scond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t buf = PTHREAD_MUTEX_INITIALIZER;

void *publisher(void *args) {
	/* TODO: The publisher will receive the following in the struct args:
	*        1. An array of mealTickets to push to the queue.
	*        2. For each meal ticket, an MTQ_ID. (Note: a publisher can push to multiple queues)
	*        3. The thread ID
	* The publisher will do the pthread_cond_wait procedure, and wait for a cond signal to begin its work
	* The publisher will then print its type and thread ID on startup. Then it will push one meal ticket at a time to
	* its appropriate queue before sleeping for 1 second. It will do this until there are no more meal tickets to push.
	*/
	
	meal = (struct MTQ *)args;
	
	printf("Publisher thread waiting %ld waiting for signal------------\n", pthread_self());
	pthread_mutex_lock(&pmutex);
	pthread_cond_wait(&cond1, &pmutex);
	pthread_mutex_unlock(&pmutex);
	
	pthread_mutex_lock (&buf);
	printf("Publisher thread: %ld enqueueing\n", pthread_self());
	enqueue(meal[i].name, meal[t%4].buffer);
	i++;
	t++;
	sched_yield();
	pthread_mutex_unlock(&buf);
	sleep(1);
	
	pthread_mutex_lock (&buf);
	printf("Publisher thread: %ld enqueueing\n", pthread_self());
	enqueue(meal[i2].name, meal[b2%4].buffer);
	i2++;
	b2++;
	sched_yield();
	pthread_mutex_unlock(&buf);
	sleep(1);
	
	pthread_mutex_lock (&buf);
	printf("Publisher thread: %ld enqueueing\n", pthread_self());
	enqueue(meal[i3].name, meal[b3%4].buffer);
	i3++;
	b3++;
	sched_yield();
	pthread_mutex_unlock(&buf);
	
}

void *subscriber(void *args) {
	/* TODO:The subscriber will take the following:
	*       1. The MTQ_ID it will pull from.
	*       2. The thread ID
	*       3. An empty meal ticket.
	* The subscriber will do the pthread_cond_wait procedure, and wait for a cond signal to begin its work
	* The subscriber will print its type and thread ID on startup. Then it will pull a ticket from its queue
	* and print it. If the queue is empty then it will print an empty message along with its
	* thread ID and wait for 1 second. If the thread is not empty then it will pop a ticket and 
	* print it along with the thread id.
	*/
	meal = (struct MTQ *)args;
	printf("subscriber thread: %ld waiting for signal-------\n", pthread_self());
	pthread_mutex_lock(&meal->lock);
	pthread_cond_wait(&cond1, &meal->lock);
	pthread_mutex_unlock(&meal->lock);
	
	printf("subscriber thread: %ld dequeueing\n", pthread_self());
			if(strcmp(meal->name, "Breakfast") == 0)
			{
				if(dequeue("Breakfast", meal->buffer->ticketNum, meal->buffer) == 0)
					printf("Queue was empty when access by subscriber thread %ld\n", pthread_self());
			}
			if(strcmp(meal->name, "Lunch") == 0)
			{
				if (dequeue("Lunch", meal->buffer->ticketNum, meal->buffer) == 0)
					printf("Queue was empty when access by subscriber thread %ld\n", pthread_self());
			}
			if(strcmp(meal->name, "Dinner") == 0)
			{
				if(dequeue("Dinner", meal->buffer->ticketNum, meal->buffer) == 0)
					printf("Queue was empty when access by subscriber thread %ld\n", pthread_self());
			}
			if(strcmp(meal->name, "Bar") == 0)
			{
				if(dequeue("Bar", meal->buffer->ticketNum, meal->buffer)== 0)
					printf("Queue was empty when access by subscriber thread %ld\n", pthread_self());
			}
			sched_yield();
			sleep(1);
	
}
//=============================================================================

//=============================== Program Main ================================
int main(int argc, char argv[]) {
	//Variables Declarations
	char *qNames[] = {"Breakfast", "Lunch", "Dinner", "Bar"};
	char *bFood[] = {"Eggs", "Bacon", "Steak"};
	char *lFood[] = {"Burger", "Fries", "Coke"};
	char *dFood[] = {"Steak", "Greens", "Pie"};
	char *brFood[] = {"Whiskey", "Sake", "Wine"};
	int i, j, t = 1;
	int test[4];
	char dsh[] = "Empty";
	mealTicket bfast[3] = {[0].dish = bFood[0], [1].dish = bFood[1], [2].dish = bFood[2]};
	mealTicket lnch[3] = {[0].dish = lFood[0], [1].dish = lFood[1], [2].dish = lFood[2]};
	mealTicket dnr[3] = {[0].dish = dFood[0], [1].dish = dFood[1], [2].dish = dFood[2]};
	mealTicket br[3] = {[0].dish = brFood[0], [1].dish = brFood[1], [2].dish = brFood[2]};
	mealTicket ticket = {.ticketNum=0, .dish=dsh};
	
	//STEP-1: Initialize the registry
	struct MTQ Breakfast;
	struct MTQ Lunch;
	struct MTQ Dinner;
	struct MTQ Bar;
	struct MTQ hold =  Breakfast;
	struct MTQ hold1 = Lunch;
	struct MTQ hold2 = Dinner;
	struct MTQ hold3 = Bar;
	registry[0] = hold;
	registry[1] = hold1;
	registry[2] = hold2;
	registry[3] = hold3;
	registry[0].head = 0;
	registry[0].tail = 0;
	registry[1].head = 0;
	registry[1].tail = 0;
	registry[2].head = 0;
	registry[2].tail = 0;
	registry[3].head = 0;
	registry[3].tail = 0;
	strcpy(registry[0].name, "Breakfast");
	registry[0].length = 3;
	strcpy(registry[1].name, "Lunch");
	registry[1].length = 3;
	strcpy(registry[2].name, "Dinner");
	registry[2].length = 3;
	strcpy(registry[3].name, "Bar");
	registry[3].length = 3;	
		

	init(0, "bfast");
	init(1, "lnch");
	init(2, "dnr");
	init(3, "br");

	registry[0].buffer = bfast;
	registry[1].buffer = lnch;
	registry[2].buffer = dnr;
	registry[3].buffer = br;

	registry[0].thread = 0;
	registry[1].thread = 1;
	registry[2].thread = 2;
	registry[3].thread = 3;
	
	//STEP-2: Create the publisher thread-pool
	
	pthread_t publisher1[4];
	pthread_t subscriber1[4];


	for(int i = 0; i < 4; i++)
	{
		pthread_create(&(publisher1[i]), NULL, publisher, registry);
	}
	for(int i = 0; i < 4; i++)
	{
		pthread_create(&(subscriber1[i]), NULL, subscriber, registry);
	}

	sleep(5);

	pthread_mutex_lock(&mutex);
	pthread_cond_broadcast(&cond1);
	pthread_mutex_unlock(&mutex);
	
	//STEP-3: Create the subscriber thread-pool
	
	//STEP-4: Join the thread-pools
	for(int i = 0; i < 4; i++)
	{
		pthread_join(subscriber1[i], NULL);
	}
	for(int i = 0; i < 4; i++)
	{
		pthread_join(publisher1[i], NULL);
	}


	//STEP-5: Free the registry
	freeMTQ(0, "bfast");
	freeMTQ(1, "lnch");
	freeMTQ(2, "dnr");
	freeMTQ(3, "br");
	
	return EXIT_SUCCESS;
}
//=============================================================================
