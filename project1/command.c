#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
/*-----------------------------------------*/

/*
int count = 1;
char cwd[1024];
getcwd(cwd, sizeof(cwd));

  //opening current working directory
DIR *dr = opendir(cwd);
struct dirent *de;
*/
void listDir()
{
  int count = 1;
  char cwd[1024];
  getcwd(cwd, sizeof(cwd));
  //opening current working directory
  DIR *dr = opendir(cwd);
  struct dirent *de;

  while ( de = readdir(dr))
  {
    printf(" %s ", de->d_name);
  }
  closedir(dr);
}

void showCurrentDir()
{
  int count = 1;
  char cwd1[1024];
  getcwd(cwd1, sizeof(cwd1));
  printf("%s", cwd1);
}

void makeDir(char *dirName)
{
  mkdir(dirName, 0777);

}

void changeDir(char *dirname)
{
	chdir(dirname);
}

void copyFile(char *sourcePath, char *destinationPath)
{
	char *buffer=(char *)malloc(1024 * sizeof(char));
	int file1;
	int file2;
	int reading;
	ssize_t count;
	if(sourcePath[strlen(sourcePath)-1] == '\n')
		sourcePath[strlen(sourcePath)-1] = 0;
	if(strcmp(destinationPath, ".") == 0 || strcmp(destinationPath, ".\n") == 0)
	{
  		const char ch = '/';
		char *ret;
		ret = strrchr(sourcePath, ch);
		ret++;
		destinationPath = ret;
	}
	file2 = open(destinationPath, O_WRONLY | O_CREAT| O_APPEND | O_NONBLOCK, 0666);
	file1 = open(sourcePath, O_RDONLY, 0666);
	if(file1 == -1 || file2 == -1){
		printf("Error! Unsupported parameters for command: cp");
		return;
	}
	if(file1 > 0)
	{
		reading = read(file1, buffer, 160);
		write(file2, buffer, 160);
	}
	close(file1);
	close(file2);
	free(buffer);	
}

void deleteFile(char *filename)
{
	if(filename[strlen(filename) - 1] == '\n')
		filename[strlen(filename)-1] = 0;
	unlink(filename);
}

void moveFile(char *sourcePath, char *destinationPath)
{
	copyFile(sourcePath, destinationPath);
	deleteFile(sourcePath);	

}


void displayFile(char *filename)
{
	int file1 = open(filename, O_RDONLY, 0666);
	int err;
	int n;
	unsigned char buffer[4096];
	while(1)
	{
		err = read(file1, buffer, sizeof(buffer));
		n = err;
		if(n==0)
			break;
		err = write(1, buffer, n);
	}
	close(file1);
}

//redirecting stdout to output.txt
 /* FILE *fp;
  fp = freopen("output.txt", "w+", stdout);
  ssize_t nread;
  size_t len = 1000;
return 1;*/
  /* while((de = readdir(dr)))
{
      size_t len = 1000;
      char *line = malloc(sizeof(char) * len);

      FILE *dif_file = fopen(de->d_name, "r");

      //excludes files in the directory that should not be written to output.txt
      //include output.txt that would cause computer to crash
      // checks for ".output.txt.swp\0" due to duplicate output.txt swap files occuring
      // checks for null character at end of each file name ("\0")

      if(strcmp(de->d_name, "command.c\0") != 0 && strcmp(de->d_name, "lab3make\0") != 0 && strcmp(de->d_name, ".output.txt.swp\0") != 0 && strcmp(de->d_name, ".\0") != 0 && strcmp(de->d_name, "..\0") != 0 && strcmp(de->d_name, "command.o\0") != 0 && strcmp(de->d_name, "command.h\0") != 0 && strcmp(de->d_name, "main.c\0") != 0 && strcmp(de->d_name, "main.o\0") != 0 && strcmp(de->d_name, "makefile\0") != 0 && strcmp(de->d_name, "a.out\0") != 0 && strcmp(de->d_name, "output.txt\0") != 0)
    {
      //writes filename followed by new line character
      write(count, "File: ", strlen("File: "));
      write(count, (de->d_name), strlen(de->d_name));
      write(count, "\n", strlen("\n"));
      int new = 1;
      while(new = 1)
      {
        //while loop to write each line of each file
        nread = getline(&line, &len, dif_file);
        //getline returns -1 once no lines remain in the file
        if(nread == -1)
        {
                new = 0;
                break;
        }
        else
        {
                char *lines = line;
                write(count, lines, strlen(lines));
                write(count, "\n", strlen("\n"));
        }
      }

      //write 80 "-" as directed following each file
      write(count, "--------------------------------------------------------------------------------", 80);
      write(count, "\n", strlen("\n"));


    }
    //closing files and free space from mallocs
    fclose(fp);
    fclose(dif_file);
    free(line);
    }

}
*/
