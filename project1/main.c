/*
*Description:
        This project has 2 modes.
	One mode takes user input. When the user types a variety of linux commands, the
 *      program exectutes those commands as UNIX would.
*	When "exit" is types, the program ends.
*	Multiple commands can be printed on a line separated by a semi-colon.
*	The user can run the program with a -f flag and a file to have the file be read
*	and executed.
*
*Author: Callista West
*
*Notes:
*       #include "command.h".
*
*/
/*-----------Preprocessor Directives-----*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include "command.h"

/*-----------------------------------------*/
/*------------Project Main----------------*/
int main(int argc, char *argv[])
{
  /* Main function Variables */
  int counter = 0;
  char *sentence;
  size_t sentsize = 1024;
  int characters;
  sentence = (char *)malloc(sentsize * sizeof(char));
  char* rest = NULL;
  int start = 1;
  char *token;
  char *tok2;
  char *rest2 = NULL;

  if(argc == 3 && strcmp(argv[1], "-f") == 0)
  {
	//moving stdout to file called output.txt
	FILE *f1;
	f1 = freopen("output.txt", "w+", stdout);
  
	//accessing file to open for program
	FILE *input = fopen(argv[2], "r");
	int s = 1;

	if(input)
	{
		while(s == 1)
		{
			getline(&sentence, &sentsize, input);
			char *newsent = sentence;
			
    		    for(token = strtok_r(newsent, ";", &newsent); token != NULL; token = strtok_r(NULL, ";", &newsent))
      			{
			if(strcmp(token, "ls ") == 0 || strcmp(token, "ls\n")== 0 || strcmp(token, " ls")==0 || strcmp(token, " ls\n")== 0)
				listDir();
			else if(strcmp(token, "pwd ") == 0 || strcmp(token, "pwd\n")== 0 || ((token[0] == ' ') && (token[1] == 'p') && (token[2] == 'w') && (token[3] == 'd')))
				showCurrentDir();
			else if((token[0] == 'm') && (token[1] == 'k') && (token[2] == 'd') && token[3]=='i' && (token[4] =='r') || (token[0] == ' ') && (token[1] == 'm') && (token[2] == 'k') && token[3]=='d' && (token[4] =='i') && (token[5] == 'r'))
			{
				int countdir = 0;
				int numpar = 0;
				for(tok2 = strtok_r(token, " ",&token); tok2 != NULL; tok2 = strtok_r(NULL, " ", &token))
				{
				   numpar += 1;
				   if(countdir < 2)
			           {
					char *newdir = tok2;
				        countdir += 1;
					if(countdir == 2)
					{
						if(newdir[strlen(newdir)-1] == '\n')
							newdir[strlen(newdir)-1] = '\0';
						makeDir(newdir);
				   		
					}
				   }
				   else
				   	printf("Error! Unrecognized command.\n");
				}
				if(numpar != 2)
					printf("Error! Incorrect syntax. No control code found.\n");
			}
			else if((token[0] == 'c') && (token[1] == 'd') || (token[0] == ' ') && (token[1] == 'c') && (token[2] == 'd'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
                                   if(countpar < 2)
                                   {
						char *cd = tok2;
                                        	countpar += 1;
					
					if(countpar == 2)
                                        {
                                                char *cd = tok2;
						if(cd[strlen(cd)-1] == '\n')
							cd[strlen(cd)-1] = '\0';
                                                changeDir(cd);

                                        }
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 2)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
			else if((token[0] == 'c') && (token[1] == 'p') || (token[0] == ' ') && (token[1] == 'c') && (token[2] == 'p'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
				   char *cd;
				   char *cd2;
                                   if(countpar < 3)
                                   {
                                        if(countpar == 1){
						cd = tok2;
                                        	countpar += 1;
					}
					else if(countpar == 2)
                                        {
                                                cd2 = tok2;
						if(cd2[strlen(cd2)-1] == '\n')
							cd2[strlen(cd2)-1] = '\0';
						copyFile(cd, cd2);
                                        }
					else
						countpar +=1;
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 3)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
			else if((token[0] == 'm') && (token[1] == 'v') || (token[0] == ' ') && (token[1] == 'm') && (token[2] == 'v'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
				   char *cd;
				   char *cd2;
                                   if(countpar < 3)
                                   {
                                        if(countpar == 1){
						cd = tok2;
                                        	countpar += 1;
					}
					else if(countpar == 2)
                                        {
                                                cd2 = tok2;
						cd2[strlen(cd2)-1] = '\0';
                                                moveFile(cd, cd2);
                                        }
					else
						countpar +=1;
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 3)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
			else if((token[0] == 'r') && (token[1] == 'm') || (token[0] == ' ') && (token[1] == 'r') && token[3] == 'm')
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
                                   if(countpar < 2)
                                   {
						char *cd = tok2;
                                        	countpar += 1;
					
					if(countpar == 2)
                                        {
                                                char *cd = tok2;
						cd[strlen(cd)-1] = '\0';
                                                deleteFile(cd);

                                        }
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 2)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
			else if((token[0] == 'c') && (token[1] == 'a') && (token[2] == 't') || (token[0] == ' ') && (token[1] == 'c') && (token[2] == 'a') && (token[3] == 't'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
                                   if(countpar < 2)
                                   {
						char *cd = tok2;
                                        	countpar += 1;
					
					if(countpar == 2)
                                        {
                                                char *cd = tok2;
						cd[strlen(cd)-1] = '\0';
                                                displayFile(cd);

                                        }
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 2)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
		}
		if(feof(input))
                {
                    //s = 0;
		    free(sentence);
                    printf("\n");
                    fclose(f1);
                    fclose(input);
                    return 1;
                }
	     }
	   }

	else 
	{
		printf("File is not found and does not exist.\n");
		return 1;
	}
  }

  //loop to keep running until "exit" is entered
  while(start == 1)
  {
        printf(">>> ");
        characters = getline(&sentence, &sentsize, stdin);
  	char *newsent = sentence;
	char *arg1 = argv[1];
	char *arg2 = argv[2];
	if(strcmp(&newsent[0], "\n") != 0)
        {
		if(strcmp(&newsent[0], "exit\n") == 0)
		{
			start == 0;
			free(sentence);
			return 1;
		}
		printf("\n");
		char *newtok;
    		for(token = strtok_r(newsent, ";", &newsent); token != NULL; token = strtok_r(NULL, ";", &newsent))
      		{
			//trying to implement error checking for "ls ls", "ls test", etc
			if(strcmp(token, "ls ") == 0 | strcmp(token, "ls\n")== 0 | strcmp(token, " ls ")==0 || strcmp(token, " ls\n")== 0)
			/*{
				int numpar = 0;
				for(tok2 = strtok_r(token, " ", &token); tok2 != NULL; tok2 = strtok_r(NULL, " ", &token))
				{
					numpar += 1;
				}
				if(numpar > 1)
					printf("Error! Incorrect syntax. No control code found.");
				else*/
					listDir();
			//}
			else if(strcmp(token, "pwd ") == 0 | strcmp(token, "pwd\n")== 0 | ((token[0] == ' ') && (token[1] == 'p') && (token[2] == 'w') && (token[3] == 'd')))
				showCurrentDir();
			else if((token[0] == 'm') && (token[1] == 'k') && (token[2] == 'd') && token[3]=='i' && (token[4] =='r') || (token[0] == ' ') && (token[1] == 'm') && (token[2] == 'k') && token[3]=='d' && (token[4] =='i') && (token[5] == 'r'))
			{
				int countdir = 0;
				int numpar = 0;
				for(tok2 = strtok_r(token, " ", &token); tok2 != NULL; tok2 = strtok_r(NULL, " ", &token))
				{
				   //printf("space separated token %s\n", tok2);
				   numpar += 1;
				   if(countdir < 2)
			           {
					char *newdir = tok2;
				        countdir += 1;
					if(countdir == 2)
					{
						if(newdir[strlen(newdir)-1] == '\n')
							newdir[strlen(newdir)-1] = '\0';
						makeDir(newdir);
				   		
					}
				   }
				   //else
				   //	printf("Error! Unrecognized command.\n");
				}
				if(numpar != 2)
					printf("Error! Incorrect syntax. No control code found.\n");
			}
			else if((token[0] == 'c') && (token[1] == 'd') || (token[0] == ' ') && (token[1] == 'c') && (token[2] == 'd'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
                                   if(countpar < 2)
                                   {
						char *cd = tok2;
                                        	countpar += 1;
					
					if(countpar == 2)
                                        {
                                                char *cd = tok2;
						if(cd[strlen(cd)-1] == '\n')
							cd[strlen(cd)-1] = '\0';
                                                changeDir(cd);

                                        }
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 2)
					printf("Error! Incorrect syntax. No control code found.");
			}
			else if((token[0] == 'c') && (token[1] == 'p') || (token[0] == ' ') && (token[1] == 'c') && (token[2] == 'p'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
				   char *cd;
				   char *cd2;
                                   if(countpar < 3)
                                   {
                                        if(countpar == 1){
						cd = tok2;
                                        	countpar += 1;
					}
					else if(countpar == 2)
                                        {
                                                cd2 = tok2;
						if(cd2[strlen(cd2)-1] == '\n')
							cd2[strlen(cd2)-1] = '\0';
						//printf("source: %s dest: %s\n", cd, cd2);
						copyFile(cd, cd2);
                                        }
					else
						countpar +=1;
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 3)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
			else if((token[0] == 'm') && (token[1] == 'v') || (token[0] == ' ') && (token[1] == 'm') && (token[2] == 'v'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
				   char *cd;
				   char *cd2;
                                   if(countpar < 3)
                                   {
                                        if(countpar == 1){
						cd = tok2;
                                        	countpar += 1;
					}
					else if(countpar == 2)
                                        {
                                                cd2 = tok2;
						cd2[strlen(cd2)-1] = '\0';
                                                moveFile(cd, cd2);
                                        }
					else
						countpar +=1;
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 3)
                                        printf("Error! Incorrect syntax. No control code found.");

			}
			else if((token[0] == 'r') && (token[1] == 'm') || (token[0] == ' ') && (token[1] == 'r') && token[3] == 'm')
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
                                   if(countpar < 2)
                                   {
						char *cd = tok2;
                                        	countpar += 1;
					
					if(countpar == 2)
                                        {
                                                char *cd = tok2;
						cd[strlen(cd)-1] = '\0';
                                                deleteFile(cd);

                                        }
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 2)
                                        printf("Error! Incorrect syntax. No control code found.");

			}
			else if((token[0] == 'c') && (token[1] == 'a') && (token[2] == 't') || (token[0] == ' ') && (token[1] == 'c') && (token[2] == 'a') && (token[3] == 't'))
			{
				int countpar = 0;
				int numpar = 0;
				for(tok2 = strtok(token, " "); tok2 != NULL; tok2 = strtok(NULL, " "))
                                {
				   numpar += 1;
                                   if(countpar < 2)
                                   {
						char *cd = tok2;
                                        	countpar += 1;
					
					if(countpar == 2)
                                        {
                                                char *cd = tok2;
						cd[strlen(cd)-1] = '\0';
                                                displayFile(cd);

                                        }
                                   }
                                   else
                                        printf("Error! Incorrect syntax. No control code found.");
                                }
				if(numpar != 2)
                                        printf("Error! Incorrect syntax. No control code found.");
			}
			else
				printf("Error! Unrecognized command: %s", token);
			if(token != NULL)
           			 printf("\n");
      		}
	}
	else
        {
                free(sentence);
                return 1;
        }
	
  
    }
 
}

//}


